// Класс является сервером обработки запросов.
// в конструкторе передается транспорт, и настройки кластеризации
// сервер может работать как в одном процессе так и порождать для обработки запросов дочерние процессы.
// Задача дописать недостающие части и отрефакторить существующий код
//
// Не важно, http/ws/tcp/ или простой сокет это все изолируется в транспорте.
// Единственное что знает сервис обработки запросов это тип подключения транспорта, постоянный или временный
// и исходя из этого создает нужную конфигурацию. ну и еще от того какой режим кластеризации был выставлен
// Необходимо реализовать ручную кластеризацию (без использования модуля cluster)
// В итоговом варианте ожидаем увидеть код в какой-либо системе контроля версия (github, gitlab) на ваш выбор
// Примеры использования при том или ином транспорте
// Будет плюсом, если задействуете в этом деле typescript и статическую типизацию.
const net = require('net')
const {fork} = require('child_process')

class Service {
    constructor({transport, isPermanentConnection, cluster = true, isMaster = true}) {
        this.transport = transport;
        this.isClusterMode = !!cluster;
        this.isMaster = isMaster;
        this.isPermanentConnection = isPermanentConnection;

        if (this.isClusterMode) {
            this.clusterOptions = cluster;
        }
        this.start()
    }

    async start() {
        if (this.isClusterMode) {
            if (this.isMaster) {
                await this.startCluster();
                if (this.isPermanentConnection) {
                    await this.startTransport();
                }
            } else {
                await this.startWorker();
                if (!this.isPermanentConnection) {
                    await this.startTransport();
                }
            }
        } else {
            await this.startWorker();
            await this.startTransport();
        }
    }

    async startTransport() {
        //todo: логика запуска транспорта
        if (this.transport instanceof net.Server) {
            this.transport.on('connection', () => console.log('asdasd'))
            this.transport.listen(5000)
        } else if (this.transport instanceof net.Socket) {
            await this.transport.connect(() => {
                console.log('server is listening')
            })

            if (this.transport.isPermanentConnection) {
                this.transport.keepAlive()
            }
        }
    }

    async startWorker() {
        //todo: логика запуска обработчика запросов
        console.log(this.transport)

    }

    async startCluster() {
        //todo: логика запуска дочерних процессов
        if (this.isMaster) {
            const forked = await fork('class.js', ['normal'])
            forked.on('message', (msg) => console.log(msg))

            this.transport.on('connection', (socket) => {
                console.log('connected')
                return forked.send('socket', socket)

            })
            forked.on('exit', (code) => {
                console.log('EXIT')
            })
        }
    }
}

process.on('message', (msg, socket) => {
    console.log(msg, socket)
    if (msg === 'socket') {
        console.log(socket)
    }
})

module.exports = Service