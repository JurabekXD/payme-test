const Service = require('./class')
const net = require('net')

new Service({
    transport: net.createServer,
    isPermanentConnection: true,
    cluster: true
})